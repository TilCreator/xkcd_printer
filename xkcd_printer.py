#! /usr/bin/env python3
#! /usr/bin/env nix-shell
#! nix-shell -i python3 -p python3 python3Packages.requests python3Packages.pyquery python3Packages.telethon python3Packages.pillow
from telethon.sync import TelegramClient
from telethon.tl.functions.messages import GetAllStickersRequest, GetArchivedStickersRequest, GetStickerSetRequest
from telethon.tl.types import InputStickerSetID
from telethon.utils import guess_extension
import logging
import time
import os
import requests
import pyquery
import traceback
import random
import config
import io
from PIL import Image
from textwrap import wrap
from piltextbox import TextBox

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

random.seed(69420)
random_list = list(range(config.xkcd_range))
random.shuffle(random_list)

with TelegramClient(config.name, config.api_id, config.api_hash) as client:
    for i in range(config.xkcd_range):
        if int(open("printer_counter", "r").read()) > i:
            continue

        i_random = random_list[i]

        break_the_loop = False
        while not break_the_loop:
            try:
                url = f"https://xkcd.com/{i_random}"
                p = pyquery.PyQuery(url=url)
                image_element = p("#comic img")
                if len(image_element) <= 0:
                    break_the_loop = True
                    break
                title = image_element.attr("alt")
                text = image_element.attr("title")
                src = image_element.attr("src")
                image = requests.get("https:" + src).content

                image_pil = Image.open(io.BytesIO(image))

                if image_pil.size[0] > image_pil.size[1]:
                    image_pil = image_pil.transpose(Image.Transpose.ROTATE_90)

                image_pil = image_pil.resize((config.paper_width_pixel, int((image_pil.size[1] / image_pil.size[0]) * config.paper_width_pixel)), resample=Image.Resampling.BICUBIC)

                def write_textbox(text_box, text_list):
                    for text in text_list:
                        if text_box.write_paragraph(text) is not None:
                            return None
                    return text_box

                text = [title, url, text]
                text_box = TextBox((image_pil.size[0], 0), typeface=config.font_path, font_size=config.font_size)

                while write_textbox(text_box, text) is None:
                    text_box = TextBox.new_same_as(text_box)
                    text_box._size = (text_box._size[0], text_box._size[1] + text_box.text_line_height)

                text_box_image = text_box.render()

                image_print = Image.new('RGB', (image_pil.size[0], image_pil.size[1] + text_box_image.size[1]))
                image_print.paste(image_pil, (0, 0))
                image_print.paste(text_box_image, (0, image_pil.size[1]))

                buf = io.BytesIO()
                image_print.save(buf, format="png")

                client.send_message(config.bot_name, config.unlock_code)
                client.send_file(config.bot_name, buf.getvalue())
                logger.info(f"send file https://xkcd.com/{i_random} {src}")

                time.sleep(11)
                if client.get_messages(config.bot_name, limit=1)[0].text != "Your sticker has finished printing now! Enjoy it :3":
                    print("anomaly detected, press enter whe resolved")
                    time.sleep(10)
                    #input()
                else:
                    print("successful print")
                    with open("printer_counter", "w") as f:
                        f.write(str(i))
                        f.flush()

                    time.sleep(10 * 60 + 10 * 60 * random.random())

                    break
            except Exception as e:
                traceback.print_exc()
                print("anomaly detected, press enter whe resolved")
                time.sleep(10)
                #input()

        #exit()
